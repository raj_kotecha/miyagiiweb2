<?php
require './bootstrap.php';

use Slim\Slim;
use Miyagiiweb\services\MiyagiiuService;
use Miyagiiweb\services\SnippetService;
use Miyagiiweb\services\AuthorService;
use Miyagiiweb\services\UserService;
use Miyagiiweb\services\FocalService;
use Miyagiiweb\services\ChallengeService;
use Miyagiiweb\services\IdeaService;
use Miyagiiweb\utils\MiyagiiSec;
use Miyagiiweb\app\EM;
use Miyagiiweb\app\RedisCloud;
use Miyagiiweb\app\AppConfig;

$app = new Slim();
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

}

$app->get('/miyagiiturbo/users', function () use ($app){
    $uc = new MiyagiiuService();
    parse_str($_SERVER['QUERY_STRING'], $queryArr);

    $txn = json_decode($queryArr['val']);
    $sec = new \Miyagiiweb\utils\MiyagiiSec();
    $isValid = $sec->validateRequest($txn);
    if ($isValid) {
        $users = $uc->allAction();
        echo "Users are here!";

        foreach ($users as $user) {
            $jsonified = json_encode($user);
            echo sprintf("%s<br/>", $jsonified);
        }
    }
});

$app->get('/miyagiiturbo/kg', function() use ($app) {
    $sec = new \Miyagiiweb\utils\MiyagiiSec();
    $timestamp = $sec->getCurrUTC14();
    $hash = $sec->generateUserHash(AppConfig::AppConfigInstance()->serverUrl() . '/miyagiiturbo/jog', $timestamp , 'alskdj2072fj', 'akjfd2lk2j3f22lkj!$');

    $txn = array(
        "url" => AppConfig::AppConfigInstance()->serverUrl() . "/miyagiiturbo/jog",
        "apiKey" => 'alskdj2072fj',
        "timestamp" => $timestamp,
        "sig" => $hash
        );

    $jsonTxn = json_encode($txn);
    $urlTxn = urlencode($jsonTxn);

    $x = 't';
});

$app->get('/miyagiiturbo/jog3489ahdf', function() use ($app) {
    parse_str($_SERVER['QUERY_STRING'], $queryArr);
    //$txn = json_decode($queryArr['val']);
    $txn = $queryArr['val'];
    //$sec = new \Miyagiiweb\utils\MiyagiiSec();
    $isValid = $txn == '39845aksjdhf84dalsf4alskdf';
    if ($isValid) {
        $testMode = array_key_exists("tck",$queryArr);

        $es = new \Miyagiiweb\services\EmailService();
        $es->jog($testMode);
    }
});

$app->post('/miyagiiturbo/authtoken', function () use ($app){
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);

    $us = new UserService(new EM());
    // check if the current user already have authenticated using this provider before
    $user_exist = $us->getUserByProviderAndId( $input->provider, $input->identifier );

    // if the used didn't authenticate using the selected provider before
    // we create a new entry on database.users for him
    if( ! $user_exist )
    {
        $us->createUser($input);
    }

    $msec = new MiyagiiSec();
    $guid = $msec->generateUserHash2($input->provider, $input->displayName, $input->identifier);
    $json = json_encode(array($input->displayName, $input->firstName, $input->identifier));

    RedisCloud::RedisClient()->set($guid, $json, "ex", 3600);

    $retval = array('authid' => $guid);
    $jsonret = json_encode($retval);
    echo $jsonret;
});

$app->post('/miyagiiturbo/snippet', function () use ($app){
    $sc = new SnippetService(new EM());
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);

    $retval = $sc->addSnippet($input);

    echo '{"success": "true"}';
});

$app->put('/miyagiiturbo/snippet', function () use ($app){
    $sc = new SnippetService(new EM());
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);

    $retval = $sc->updateSnippet($input);

    echo '{"success": "true"}';
});

$app->post('/miyagiiturbo/snippetX', function () use ($app){
    $sc = new SnippetService(new EM());
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);

    $retval = $sc->addSnippetX($input);

    //echo '{"success": "true"}';
    echo json_encode($retval[1]);
});

$app->delete('/miyagiiturbo/dels', function () use ($app){
    $sc = new SnippetService(new EM());
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);

    $retval = $sc->deleteSnippet($input);
});

$app->post('/miyagiiturbo/user', function () use ($app){
    $us = new UserService(new EM());
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);

    $retval = $us->createUser($input);
    echo json_encode($retval);
});

$app->post('/miyagiiturbo/author', function () use ($app) {
    $sc = new AuthorService(new EM());
    //TODO: Add security logic
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);

    $author = $sc->getAuthorsByURL($input->page_url);
    if(!empty($author))
        echo $author[0]->getAuthorName();
});

$app->get('/miyagiiturbo/snippet', function () use ($app) {
    $sc = new SnippetService(new EM());
    //TODO: Add security logic
    parse_str($_SERVER['QUERY_STRING'], $queryArr);

    $id = $queryArr['snippet_id'];
    $user = $queryArr['auth'];
    $userinfo = json_decode(RedisCloud::RedisClient()->get($user));
    $snippets = $sc->getSnippet($id, $userinfo);

    $snippetsX = [];

    foreach ($snippets as $snippet) {
        //$snippet->setSnippetDetail(nl2br($snippet->getSnippetDetail()));
        $snippet["snippetDetail"] = nl2br($snippet["snippetDetail"]);
        $snippet["snippetComment"] = nl2br($snippet["snippetComment"]);
        array_push($snippetsX, $snippet);
    }


    echo json_encode($snippetsX);
});

$app->get('/miyagiiturbo/snippets', function () use ($app) {
    $sc = new SnippetService(new EM());
    //TODO: Add security logic
    parse_str($_SERVER['QUERY_STRING'], $queryArr);

    $id = $queryArr['auth'];
    $start = 0;
    if(array_key_exists('start', $queryArr)) $start = $queryArr['start'];

    $userinfo = json_decode(RedisCloud::RedisClient()->get($id));

    $snippets = $sc->getSnippetsByUser($userinfo, $start);

    echo json_encode($snippets);
});

$app->get('/miyagiiturbo/snippetsw', function () use ($app) {
    $sc = new SnippetService(new EM());
    $tc = new \Miyagiiweb\services\TagService(new EM());
    //TODO: Add security logic
    parse_str($_SERVER['QUERY_STRING'], $queryArr);

    $id = $queryArr['auth'];
    $start = 0;
    if(array_key_exists('start', $queryArr)) $start = $queryArr['start'];

    $fmt = "web";
    if(array_key_exists('fmt', $queryArr)) $fmt = $queryArr['fmt'];

    $userinfo = json_decode(RedisCloud::RedisClient()->get($id));

    if($userinfo == null) {
        $noUser = array(
            "status" => 0,
            "message" => "something went wrong, try logging in again"
        );
        echo json_encode($noUser);
        return;
    }

    $snippets = $sc->getSnippetsByUser($userinfo, $start);
    $snippetsX = [];

    if($fmt == "web") {
        foreach ($snippets as $snippet) {
          $snippet["snippetDetail"] = nl2br($snippet["snippetDetail"]);
          $snippet["snippetComment"] = nl2br($snippet["snippetComment"]);
          $tags = $tc->getSnippetTags($snippet["snippetId"]);
          $a = array_map(function($obj) { return "#" . $obj['tagName']; }, $tags);
          $snippet["tags"] = implode(", ", $a);
            //$x = $snippet["snippetCreatedAt"];
            //$y = $x->format('l, M j Y, g:ia');
            //$snippet["snippetCreatedAt"] = $y;

            array_push($snippetsX, $snippet);
        }
        echo json_encode($snippetsX);
    }
    else {
        echo json_encode($snippets);
    }
});

$app->get('/miyagiiturbo/search', function() use ($app) {
    $sc = new SnippetService(new EM());
    parse_str($_SERVER['QUERY_STRING'], $queryArr);
    $id = $queryArr['auth'];
    $tags = $queryArr['tags'];
    $userinfo = json_decode(RedisCloud::RedisClient()->get($id));

    $snippets = $sc->getSnippetsByUserTagsX($userinfo, $tags);
    $snippetsX = [];

    foreach ($snippets as $snippet) {
        $snippet["snippetDetail"] = nl2br($snippet["snippetDetail"]);
        $snippet["snippetComment"] = nl2br($snippet["snippetComment"]);
        array_push($snippetsX, $snippet);
    }

    echo json_encode($snippetsX);
});

$app->get('/miyagiiturbo/tags', function() use($app) {
    $tc = new \Miyagiiweb\services\TagService(new EM());
    parse_str($_SERVER['QUERY_STRING'], $queryArr);
    $id = $queryArr['auth'];
    $userinfo = json_decode(RedisCloud::RedisClient()->get($id));

    $tags = $tc->getTagsForUser($userinfo);
    echo json_encode($tags);

});

$app->get('/miyagiiturbo/stags', function() use($app) {
    $tc = new \Miyagiiweb\services\TagService(new EM());
    parse_str($_SERVER['QUERY_STRING'], $queryArr);
    $id = $queryArr['auth'];
    $sid = $queryArr['snippet_id'];

    //TODO: Security check
    $userinfo = json_decode(RedisCloud::RedisClient()->get($id));

    $tags = $tc->getSnippetTags($sid);
    echo json_encode($tags);

});

/* <-- Focal methods */
$app->get('/miyagiiturbo/focal', function() use ($app) {
    $fs = new FocalService(new EM());
    parse_str($_SERVER['QUERY_STRING'], $queryArr);
    $id = $queryArr['auth'];
    $focalId = $queryArr['fid'];
    $userinfo = json_decode(RedisCloud::RedisClient()->get($id));

    $focal = $fs->getUserFocalById($userinfo, $focalId);
    echo json_encode($focal);
});

$app->post('/miyagiiturbo/focal', function () use ($app){

    $fs = new FocalService(new EM());
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);

    $retval = $fs->createFocal($input->focusName, $input->userinfo);

    echo '{"success": "true"}';

});

$app->post('/miyagiiturbo/challenge', function () use ($app){

    $cs = new ChallengeService(new EM());
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);

    $retval = $cs->createChallenge($input);

    echo '{"success": "true"}';

});

$app->get('/miyagiiturbo/challenges', function() use($app) {
    $cs = new ChallengeService(new EM());
    parse_str($_SERVER['QUERY_STRING'], $queryArr);

    $id = $queryArr['auth'];
    $snippetId = $queryArr['sid'];

    $userinfo = json_decode(RedisCloud::RedisClient()->get($id));

    $challenges = $cs->getChallengesBySnippet($userinfo, $snippetId);

    echo json_encode($challenges);
});

$app->get('/miyagiiturbo/challenge', function() use ($app) {
    $cs = new ChallengeService(new EM());
    parse_str($_SERVER['QUERY_STRING'], $queryArr);

    $id = $queryArr['auth'];
    $challengeId = $queryArr['cid'];

    $userinfo = json_decode(RedisCloud::RedisClient()->get($id));

    $challenges = $cs->getUserChallengeById($userinfo, $challengeId);

    echo json_encode($challenges);
});


$app->get('/miyagiiturbo/focals', function() use($app) {
    $fs = new FocalService(new EM());
    parse_str($_SERVER['QUERY_STRING'], $queryArr);
    $id = $queryArr['auth'];
    $userinfo = json_decode(RedisCloud::RedisClient()->get($id));

    $focals = $fs->getFocalsForUser($userinfo);
    echo json_encode($focals);

});

$app->post('/miyagiiturbo/idea', function() use($app) {
    //TODO: Error handling logic
    $is = new IdeaService(new EM());
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);
    $retval = $is->addIdea($input);
    echo '{"success":"true"}';
});

$app->get('/miyagiiturbo/ideas', function() use($app) {
    $is = new IdeaService(new EM());
    parse_str($_SERVER['QUERY_STRING'], $queryArr);
    $id = $queryArr['auth'];
    $challengeId = $queryArr['cid'];

    $userinfo = json_decode(RedisCloud::RedisClient()->get($id));

    $ideas = $is->getIdeasForChallenge($userinfo, $challengeId);
    echo json_encode($ideas);
});

/* <-- end Focal methods */

// return HTTP 200 for HTTP OPTIONS requests
$app->map('/:x+', function($x) {
    http_response_code(200);
})->via('OPTIONS');


$app->run();