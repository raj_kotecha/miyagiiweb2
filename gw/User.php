<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="IDX_username", columns={"username"})})
 * @ORM\Entity
 */
class User extends \MiyagiiEntityBase
{
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_created_at", type="datetime", nullable=false)
     */
    private $userCreatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="user_email", type="string", length=100, nullable=true)
     */
    private $userEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_name", type="string", length=45, nullable=true)
     */
    private $providerName;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_id", type="string", length=100, nullable=true)
     */
    private $providerId;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_display_name", type="string", length=45, nullable=true)
     */
    private $providerDisplayName;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_firstname", type="string", length=45, nullable=true)
     */
    private $providerFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_lastname", type="string", length=45, nullable=true)
     */
    private $providerLastname;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userId;


    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set userCreatedAt
     *
     * @param \DateTime $userCreatedAt
     * @return User
     */
    public function setUserCreatedAt($userCreatedAt)
    {
        $this->userCreatedAt = $userCreatedAt;

        return $this;
    }

    /**
     * Get userCreatedAt
     *
     * @return \DateTime 
     */
    public function getUserCreatedAt()
    {
        return $this->userCreatedAt;
    }

    /**
     * Set userEmail
     *
     * @param string $userEmail
     * @return User
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Get userEmail
     *
     * @return string 
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * Set providerName
     *
     * @param string $providerName
     * @return User
     */
    public function setProviderName($providerName)
    {
        $this->providerName = $providerName;

        return $this;
    }

    /**
     * Get providerName
     *
     * @return string 
     */
    public function getProviderName()
    {
        return $this->providerName;
    }

    /**
     * Set providerId
     *
     * @param string $providerId
     * @return User
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;

        return $this;
    }

    /**
     * Get providerId
     *
     * @return string 
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * Set providerDisplayName
     *
     * @param string $providerDisplayName
     * @return User
     */
    public function setProviderDisplayName($providerDisplayName)
    {
        $this->providerDisplayName = $providerDisplayName;

        return $this;
    }

    /**
     * Get providerDisplayName
     *
     * @return string 
     */
    public function getProviderDisplayName()
    {
        return $this->providerDisplayName;
    }

    /**
     * Set providerFirstname
     *
     * @param string $providerFirstname
     * @return User
     */
    public function setProviderFirstname($providerFirstname)
    {
        $this->providerFirstname = $providerFirstname;

        return $this;
    }

    /**
     * Get providerFirstname
     *
     * @return string 
     */
    public function getProviderFirstname()
    {
        return $this->providerFirstname;
    }

    /**
     * Set providerLastname
     *
     * @param string $providerLastname
     * @return User
     */
    public function setProviderLastname($providerLastname)
    {
        $this->providerLastname = $providerLastname;

        return $this;
    }

    /**
     * Get providerLastname
     *
     * @return string 
     */
    public function getProviderLastname()
    {
        return $this->providerLastname;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }
    /**
     * @var integer
     *
     * @ORM\Column(name="user_email_flag", type="integer", nullable=false)
     */
    protected $userEmailFlag = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="user_auto_action", type="integer", nullable=false)
     */
    protected $userAutoAction = '0';


    /**
     * Set userEmailFlag
     *
     * @param integer $userEmailFlag
     * @return User
     */
    public function setUserEmailFlag($userEmailFlag)
    {
        $this->userEmailFlag = $userEmailFlag;

        return $this;
    }

    /**
     * Get userEmailFlag
     *
     * @return integer 
     */
    public function getUserEmailFlag()
    {
        return $this->userEmailFlag;
    }

    /**
     * Set userAutoAction
     *
     * @param integer $userAutoAction
     * @return User
     */
    public function setUserAutoAction($userAutoAction)
    {
        $this->userAutoAction = $userAutoAction;

        return $this;
    }

    /**
     * Get userAutoAction
     *
     * @return integer 
     */
    public function getUserAutoAction()
    {
        return $this->userAutoAction;
    }
}
