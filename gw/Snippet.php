<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Snippet
 *
 * @ORM\Table(name="snippet", indexes={@ORM\Index(name="FK_AUTHOR_idx", columns={"snippet_author"}), @ORM\Index(name="FK_USER_idx", columns={"user_id"})})
 * @ORM\Entity
 */
class Snippet extends \MiyagiiEntityBase
{
    /**
     * @var string
     *
     * @ORM\Column(name="snippet_pageurl", type="string", length=2083, nullable=false)
     */
    protected $snippetPageurl;

    /**
     * @var string
     *
     * @ORM\Column(name="snippet_detail", type="text", nullable=false)
     */
    protected $snippetDetail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="snippet_created_at", type="datetime", nullable=false)
     */
    protected $snippetCreatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="snippet_comment", type="text", nullable=true)
     */
    protected $snippetComment;

    /**
     * @var integer
     *
     * @ORM\Column(name="snippet_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $snippetId;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    protected $user;

    /**
     * @var \Author
     *
     * @ORM\ManyToOne(targetEntity="Author")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="snippet_author", referencedColumnName="author_id")
     * })
     */
    protected $snippetAuthor;


    /**
     * Set snippetPageurl
     *
     * @param string $snippetPageurl
     * @return Snippet
     */
    public function setSnippetPageurl($snippetPageurl)
    {
        $this->snippetPageurl = $snippetPageurl;

        return $this;
    }

    /**
     * Get snippetPageurl
     *
     * @return string 
     */
    public function getSnippetPageurl()
    {
        return $this->snippetPageurl;
    }

    /**
     * Set snippetDetail
     *
     * @param string $snippetDetail
     * @return Snippet
     */
    public function setSnippetDetail($snippetDetail)
    {
        $this->snippetDetail = $snippetDetail;

        return $this;
    }

    /**
     * Get snippetDetail
     *
     * @return string 
     */
    public function getSnippetDetail()
    {
        return $this->snippetDetail;
    }

    /**
     * Set snippetCreatedAt
     *
     * @param \DateTime $snippetCreatedAt
     * @return Snippet
     */
    public function setSnippetCreatedAt($snippetCreatedAt)
    {
        $this->snippetCreatedAt = $snippetCreatedAt;

        return $this;
    }

    /**
     * Get snippetCreatedAt
     *
     * @return \DateTime 
     */
    public function getSnippetCreatedAt()
    {
        return $this->snippetCreatedAt;
    }

    /**
     * Set snippetComment
     *
     * @param string $snippetComment
     * @return Snippet
     */
    public function setSnippetComment($snippetComment)
    {
        $this->snippetComment = $snippetComment;

        return $this;
    }

    /**
     * Get snippetComment
     *
     * @return string 
     */
    public function getSnippetComment()
    {
        return $this->snippetComment;
    }

    /**
     * Get snippetId
     *
     * @return integer 
     */
    public function getSnippetId()
    {
        return $this->snippetId;
    }

    /**
     * Set user
     *
     * @param \User $user
     * @return Snippet
     */
    public function setUser(\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set snippetAuthor
     *
     * @param \Author $snippetAuthor
     * @return Snippet
     */
    public function setSnippetAuthor(\Author $snippetAuthor = null)
    {
        $this->snippetAuthor = $snippetAuthor;

        return $this;
    }

    /**
     * Get snippetAuthor
     *
     * @return \Author 
     */
    public function getSnippetAuthor()
    {
        return $this->snippetAuthor;
    }
}
