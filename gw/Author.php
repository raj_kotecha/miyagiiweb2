<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Author
 *
 * @ORM\Table(name="author")
 * @ORM\Entity
 */
class Author extends \MiyagiiEntityBase
{
    /**
     * @var string
     *
     * @ORM\Column(name="author_name", type="string", length=45, nullable=true)
     */
    protected $authorName;

    /**
     * @var string
     *
     * @ORM\Column(name="author_url", type="string", length=2083, nullable=true)
     */
    protected $authorUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="author_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $authorId;


    /**
     * Set authorName
     *
     * @param string $authorName
     * @return Author
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;

        return $this;
    }

    /**
     * Get authorName
     *
     * @return string 
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * Set authorUrl
     *
     * @param string $authorUrl
     * @return Author
     */
    public function setAuthorUrl($authorUrl)
    {
        $this->authorUrl = $authorUrl;

        return $this;
    }

    /**
     * Get authorUrl
     *
     * @return string 
     */
    public function getAuthorUrl()
    {
        return $this->authorUrl;
    }

    /**
     * Get authorId
     *
     * @return integer 
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="author_created", type="datetime", nullable=false)
     */
    protected $authorCreated;


    /**
     * Set authorCreated
     *
     * @param \DateTime $authorCreated
     * @return Author
     */
    public function setAuthorCreated($authorCreated)
    {
        $this->authorCreated = $authorCreated;

        return $this;
    }

    /**
     * Get authorCreated
     *
     * @return \DateTime 
     */
    public function getAuthorCreated()
    {
        return $this->authorCreated;
    }
}
