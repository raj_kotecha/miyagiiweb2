<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Idea
 *
 * @ORM\Table(name="idea", indexes={@ORM\Index(name="FK_USER_IDEA_idx", columns={"user_id"})})
 * @ORM\Entity
 */
class Idea extends \MiyagiiEntityBase
{
    /**
     * @var string
     *
     * @ORM\Column(name="idea_title", type="string", length=60, nullable=false)
     */
    protected $ideaTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="idea_rating", type="integer", nullable=false)
     */
    protected $ideaRating;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="idea_created_at", type="datetime", nullable=false)
     */
    protected $ideaCreatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="idea_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $ideaId;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    protected $user;


    /**
     * Set ideaTitle
     *
     * @param string $ideaTitle
     * @return Idea
     */
    public function setIdeaTitle($ideaTitle)
    {
        $this->ideaTitle = $ideaTitle;

        return $this;
    }

    /**
     * Get ideaTitle
     *
     * @return string 
     */
    public function getIdeaTitle()
    {
        return $this->ideaTitle;
    }

    /**
     * Set ideaRating
     *
     * @param integer $ideaRating
     * @return Idea
     */
    public function setIdeaRating($ideaRating)
    {
        $this->ideaRating = $ideaRating;

        return $this;
    }

    /**
     * Get ideaRating
     *
     * @return integer 
     */
    public function getIdeaRating()
    {
        return $this->ideaRating;
    }

    /**
     * Set ideaCreatedAt
     *
     * @param \DateTime $ideaCreatedAt
     * @return Idea
     */
    public function setIdeaCreatedAt($ideaCreatedAt)
    {
        $this->ideaCreatedAt = $ideaCreatedAt;

        return $this;
    }

    /**
     * Get ideaCreatedAt
     *
     * @return \DateTime 
     */
    public function getIdeaCreatedAt()
    {
        return $this->ideaCreatedAt;
    }

    /**
     * Get ideaId
     *
     * @return integer 
     */
    public function getIdeaId()
    {
        return $this->ideaId;
    }

    /**
     * Set user
     *
     * @param \User $user
     * @return Idea
     */
    public function setUser(\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
