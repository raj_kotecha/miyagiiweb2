<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * UserSnippet
 *
 * @ORM\Table(name="user_snippet", indexes={@ORM\Index(name="FK_USERID_idx", columns={"user_id"}), @ORM\Index(name="FK_LESSONID_idx", columns={"snippet_id"})})
 * @ORM\Entity
 */
class UserSnippet extends \MiyagiiEntityBase
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_snippet_created_at", type="datetime", nullable=false)
     */
    protected $userSnippetCreatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_snippet_owner_id", type="integer", nullable=false)
     */
    protected $userSnippetOwnerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_snippet_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $userSnippetId;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    protected $user;

    /**
     * @var \Snippet
     *
     * @ORM\ManyToOne(targetEntity="Snippet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="snippet_id", referencedColumnName="snippet_id")
     * })
     */
    protected $snippet;


    /**
     * Set userSnippetCreatedAt
     *
     * @param \DateTime $userSnippetCreatedAt
     * @return UserSnippet
     */
    public function setUserSnippetCreatedAt($userSnippetCreatedAt)
    {
        $this->userSnippetCreatedAt = $userSnippetCreatedAt;

        return $this;
    }

    /**
     * Get userSnippetCreatedAt
     *
     * @return \DateTime 
     */
    public function getUserSnippetCreatedAt()
    {
        return $this->userSnippetCreatedAt;
    }

    /**
     * Set userSnippetOwnerId
     *
     * @param integer $userSnippetOwnerId
     * @return UserSnippet
     */
    public function setUserSnippetOwnerId($userSnippetOwnerId)
    {
        $this->userSnippetOwnerId = $userSnippetOwnerId;

        return $this;
    }

    /**
     * Get userSnippetOwnerId
     *
     * @return integer 
     */
    public function getUserSnippetOwnerId()
    {
        return $this->userSnippetOwnerId;
    }

    /**
     * Get userSnippetId
     *
     * @return integer 
     */
    public function getUserSnippetId()
    {
        return $this->userSnippetId;
    }

    /**
     * Set user
     *
     * @param \User $user
     * @return UserSnippet
     */
    public function setUser(\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set snippet
     *
     * @param \Snippet $snippet
     * @return UserSnippet
     */
    public function setSnippet(\Snippet $snippet = null)
    {
        $this->snippet = $snippet;

        return $this;
    }

    /**
     * Get snippet
     *
     * @return \Snippet 
     */
    public function getSnippet()
    {
        return $this->snippet;
    }
}
