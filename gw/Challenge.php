<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Challenge
 *
 * @ORM\Table(name="challenge", indexes={@ORM\Index(name="FK_CHALLENGE_TYPE_idx", columns={"challenge_type_id"}), @ORM\Index(name="FK_USER_CHALLENGE_idx", columns={"user_id"}), @ORM\Index(name="FK_CHALLENGE_FOCUS_idx", columns={"focus_id"}), @ORM\Index(name="FK_CHALLENGE_SNIPPET_idx", columns={"snippet_id"})})
 * @ORM\Entity
 */
class Challenge extends \MiyagiiEntityBase
{
    /**
     * @var string
     *
     * @ORM\Column(name="challenge_description", type="text", nullable=false)
     */
    protected $challengeDescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="challenge_created_at", type="datetime", nullable=false)
     */
    protected $challengeCreatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="challenge_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $challengeId;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    protected $user;

    /**
     * @var \ChallengeType
     *
     * @ORM\ManyToOne(targetEntity="ChallengeType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="challenge_type_id", referencedColumnName="challenge_type_id")
     * })
     */
    protected $challengeType;

    /**
     * @var \Focus
     *
     * @ORM\ManyToOne(targetEntity="Focus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="focus_id", referencedColumnName="focus_id")
     * })
     */
    protected $focus;

    /**
     * @var \Snippet
     *
     * @ORM\ManyToOne(targetEntity="Snippet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="snippet_id", referencedColumnName="snippet_id")
     * })
     */
    protected $snippet;


    /**
     * Set challengeDescription
     *
     * @param string $challengeDescription
     * @return Challenge
     */
    public function setChallengeDescription($challengeDescription)
    {
        $this->challengeDescription = $challengeDescription;

        return $this;
    }

    /**
     * Get challengeDescription
     *
     * @return string 
     */
    public function getChallengeDescription()
    {
        return $this->challengeDescription;
    }

    /**
     * Set challengeCreatedAt
     *
     * @param \DateTime $challengeCreatedAt
     * @return Challenge
     */
    public function setChallengeCreatedAt($challengeCreatedAt)
    {
        $this->challengeCreatedAt = $challengeCreatedAt;

        return $this;
    }

    /**
     * Get challengeCreatedAt
     *
     * @return \DateTime 
     */
    public function getChallengeCreatedAt()
    {
        return $this->challengeCreatedAt;
    }

    /**
     * Get challengeId
     *
     * @return integer 
     */
    public function getChallengeId()
    {
        return $this->challengeId;
    }

    /**
     * Set user
     *
     * @param \User $user
     * @return Challenge
     */
    public function setUser(\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set challengeType
     *
     * @param \ChallengeType $challengeType
     * @return Challenge
     */
    public function setChallengeType(\ChallengeType $challengeType = null)
    {
        $this->challengeType = $challengeType;

        return $this;
    }

    /**
     * Get challengeType
     *
     * @return \ChallengeType 
     */
    public function getChallengeType()
    {
        return $this->challengeType;
    }

    /**
     * Set focus
     *
     * @param \Focus $focus
     * @return Challenge
     */
    public function setFocus(\Focus $focus = null)
    {
        $this->focus = $focus;

        return $this;
    }

    /**
     * Get focus
     *
     * @return \Focus 
     */
    public function getFocus()
    {
        return $this->focus;
    }

    /**
     * Set snippet
     *
     * @param \Snippet $snippet
     * @return Challenge
     */
    public function setSnippet(\Snippet $snippet = null)
    {
        $this->snippet = $snippet;

        return $this;
    }

    /**
     * Get snippet
     *
     * @return \Snippet 
     */
    public function getSnippet()
    {
        return $this->snippet;
    }
    /**
     * @var integer
     *
     * @ORM\Column(name="challenge_status", type="integer", nullable=false)
     */
    protected $challengeStatus;


    /**
     * Set challengeStatus
     *
     * @param integer $challengeStatus
     * @return Challenge
     */
    public function setChallengeStatus($challengeStatus)
    {
        $this->challengeStatus = $challengeStatus;

        return $this;
    }

    /**
     * Get challengeStatus
     *
     * @return integer 
     */
    public function getChallengeStatus()
    {
        return $this->challengeStatus;
    }
}
