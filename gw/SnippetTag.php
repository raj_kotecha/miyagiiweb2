<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * SnippetTag
 *
 * @ORM\Table(name="snippet_tag", indexes={@ORM\Index(name="UNIQUE_IDX", columns={"tag_id", "snippet_id"}), @ORM\Index(name="snippet_id_idx", columns={"snippet_id"}), @ORM\Index(name="IDX_485C6E1ABAD26311", columns={"tag_id"})})
 * @ORM\Entity
 */
class SnippetTag extends \MiyagiiEntityBase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="snippet_tag_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $snippetTagId;

    /**
     * @var \Tag
     *
     * @ORM\ManyToOne(targetEntity="Tag")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tag_id", referencedColumnName="tag_id")
     * })
     */
    protected $tag;

    /**
     * @var \Snippet
     *
     * @ORM\ManyToOne(targetEntity="Snippet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="snippet_id", referencedColumnName="snippet_id")
     * })
     */
    protected $snippet;


    /**
     * Get snippetTagId
     *
     * @return integer 
     */
    public function getSnippetTagId()
    {
        return $this->snippetTagId;
    }

    /**
     * Set tag
     *
     * @param \Tag $tag
     * @return SnippetTag
     */
    public function setTag(\Tag $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \Tag 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set snippet
     *
     * @param \Snippet $snippet
     * @return SnippetTag
     */
    public function setSnippet(\Snippet $snippet = null)
    {
        $this->snippet = $snippet;

        return $this;
    }

    /**
     * Get snippet
     *
     * @return \Snippet 
     */
    public function getSnippet()
    {
        return $this->snippet;
    }
}
