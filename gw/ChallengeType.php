<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ChallengeType
 *
 * @ORM\Table(name="challenge_type")
 * @ORM\Entity
 */
class ChallengeType extends \MiyagiiEntityBase
{
    /**
     * @var string
     *
     * @ORM\Column(name="challenge_type_desc", type="string", length=20, nullable=false)
     */
    protected $challengeTypeDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="challenge_type_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $challengeTypeId;


    /**
     * Set challengeTypeDesc
     *
     * @param string $challengeTypeDesc
     * @return ChallengeType
     */
    public function setChallengeTypeDesc($challengeTypeDesc)
    {
        $this->challengeTypeDesc = $challengeTypeDesc;

        return $this;
    }

    /**
     * Get challengeTypeDesc
     *
     * @return string 
     */
    public function getChallengeTypeDesc()
    {
        return $this->challengeTypeDesc;
    }

    /**
     * Get challengeTypeId
     *
     * @return integer 
     */
    public function getChallengeTypeId()
    {
        return $this->challengeTypeId;
    }
}
