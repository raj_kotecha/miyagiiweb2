<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Focus
 *
 * @ORM\Table(name="focus", indexes={@ORM\Index(name="FK_FOCUS_USER_idx", columns={"user_id"})})
 * @ORM\Entity
 */
class Focus extends \MiyagiiEntityBase
{
    /**
     * @var string
     *
     * @ORM\Column(name="focus_name", type="string", length=45, nullable=false)
     */
    protected $focusName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="focus_created_at", type="datetime", nullable=false)
     */
    protected $focusCreatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="focus_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $focusId;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    protected $user;


    /**
     * Set focusName
     *
     * @param string $focusName
     * @return Focus
     */
    public function setFocusName($focusName)
    {
        $this->focusName = $focusName;

        return $this;
    }

    /**
     * Get focusName
     *
     * @return string 
     */
    public function getFocusName()
    {
        return $this->focusName;
    }

    /**
     * Set focusCreatedAt
     *
     * @param \DateTime $focusCreatedAt
     * @return Focus
     */
    public function setFocusCreatedAt($focusCreatedAt)
    {
        $this->focusCreatedAt = $focusCreatedAt;

        return $this;
    }

    /**
     * Get focusCreatedAt
     *
     * @return \DateTime 
     */
    public function getFocusCreatedAt()
    {
        return $this->focusCreatedAt;
    }

    /**
     * Get focusId
     *
     * @return integer 
     */
    public function getFocusId()
    {
        return $this->focusId;
    }

    /**
     * Set user
     *
     * @param \User $user
     * @return Focus
     */
    public function setUser(\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
