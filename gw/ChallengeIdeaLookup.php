<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * ChallengeIdeaLookup
 *
 * @ORM\Table(name="challenge_idea_lookup", indexes={@ORM\Index(name="FK_CHALLENGE_idx", columns={"challenge_id"}), @ORM\Index(name="FK_CI_IDEA_idx", columns={"idea_id"})})
 * @ORM\Entity
 */
class ChallengeIdeaLookup extends \MiyagiiEntityBase
{
    /**
     * @var integer
     *
     * @ORM\Column(name="challenge_idea_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $challengeIdeaId;

    /**
     * @var \Challenge
     *
     * @ORM\ManyToOne(targetEntity="Challenge")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="challenge_id", referencedColumnName="challenge_id")
     * })
     */
    protected $challenge;

    /**
     * @var \Idea
     *
     * @ORM\ManyToOne(targetEntity="Idea")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idea_id", referencedColumnName="idea_id")
     * })
     */
    protected $idea;


    /**
     * Get challengeIdeaId
     *
     * @return integer 
     */
    public function getChallengeIdeaId()
    {
        return $this->challengeIdeaId;
    }

    /**
     * Set challenge
     *
     * @param \Challenge $challenge
     * @return ChallengeIdeaLookup
     */
    public function setChallenge(\Challenge $challenge = null)
    {
        $this->challenge = $challenge;

        return $this;
    }

    /**
     * Get challenge
     *
     * @return \Challenge 
     */
    public function getChallenge()
    {
        return $this->challenge;
    }

    /**
     * Set idea
     *
     * @param \Idea $idea
     * @return ChallengeIdeaLookup
     */
    public function setIdea(\Idea $idea = null)
    {
        $this->idea = $idea;

        return $this;
    }

    /**
     * Get idea
     *
     * @return \Idea 
     */
    public function getIdea()
    {
        return $this->idea;
    }
}
