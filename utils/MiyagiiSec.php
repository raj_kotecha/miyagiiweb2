<?php
/**
 * Created by PhpStorm.
 * User: raj
 * Date: 2014-09-28
 * Time: 11:15 AM
 */

namespace Miyagiiweb\utils;

use Miyagiiweb\services\MiyagiiuService;

class MiyagiiSec {
    /*
     * HMAC will consist of:
     * ENCRYPTED (with private key which is never transmitted)
     * - full URL including parameters and path except http or https
     * - timestamp + - 15mins
     * UNENCRYPTED:
     * - full URL including parameters and path except http or https
     * - username / api key which server will use to find private key
     */

    public function generateUserHash2($provider, $displayname, $providerid) {
        $hashsig = $this->safe_base64_encode(hash_hmac('sha256', $provider . $displayname . $providerid . $this->getCurrUTC14(), "m8rq8bl38ppl,mko0918", true));
        return $hashsig;
    }

    public function validateRequest($request) {
        return $this->authRequest($request) && $this->checkRequestTiming(1);
    }

    //TODO: implement this function!
    private function checkRequestTiming($user_utc14) {
        return $user_utc14 === $user_utc14;
    }

    private function authRequest($request) {
        $uc = new MiyagiiuService();

        //Get key components from the request like the APIKey, user, hashedsig, url, timestamp, etc
        //Retrieve the private key of the user from the database. This is the private that would have
        //been used to encrypt the hashed sig on the client
        $apiKey = $request->apiKey;
        $user = $uc->getUser($apiKey);
        $hashedValue = $request->sig;
        $url = $request->url;
        $timestamp = $request->timestamp;
        $prikey = $user->getApiPKey();

        //Generate the same hashed sig on the server
        $hashsig_expected = $this->generateUserHash($url, $timestamp, $apiKey, $prikey);

        //Do a comparison of the passed in sig to the one generated on the server
        //If they match you've verified the private key ...
        $isMatch = $this->checkHash($hashedValue, $hashsig_expected);
        return $isMatch;
    }

    public function generateUserHash($url, $timestamp, $pukey, $prikey) {
        $utc14 = $this->getCurrUTC14();
        $hashsig = base64_encode(hash_hmac('sha256', $url . $pukey . $timestamp, $prikey, true));
        return $hashsig;
    }

    public function getCurrUTC14() {
        // set timezone to UTC timezone
        $curr_tz = date_default_timezone_get();
        date_default_timezone_set("UTC");

        $dt = new \DateTime('now');

        $UTC14 = $dt->format('YmdHis');

        // return timezone to server default
        date_default_timezone_set($curr_tz);

        return $UTC14;
    }

    private function checkHash ($hashsig, $hashsig_expected) {
        return md5($hashsig) === md5($hashsig_expected);
    }

    private function safe_base64_encode($value) {
        if (!is_string($value)) {
            return false;
        }
        return str_replace(array('+', '/', '='), array('a', 'b', 'c'), base64_encode($value));
    }
} 