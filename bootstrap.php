<?php namespace Miyagiiweb\app;
// bootstrap.php - need to recommit....


require_once "./vendor/autoload.php";
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Predis\Autoloader;
use Predis\Client;

//echo "found bootstrap ";

class AppConfig {
    private $env;
    private $isRemote = false;
    private static $AppConfig;

    private function __construct(){

        if (array_key_exists('HTTP_HOST', $_SERVER)) {
            $http_host = $_SERVER['HTTP_HOST'];
            if (strpos($http_host, 'localhost') === false && strpos($http_host, '192') === false) {
                //$_SERVER['SERVER_PORT'] = 80;
                $this->isRemote = true;
                $this->env = "prod";
            } else {
                $this->env = "dev";
                //echo "detected dev";
            }
        }
        else {
            $this->env = "dev";
            //echo "\ndetected dev\n";
        }
    }

    public static function AppConfigInstance($env = "dev"){
        if (AppConfig::$AppConfig == null) {
            self::$AppConfig = new AppConfig($env);
        }
        return self::$AppConfig;
    }

    public function Environment() {
        return $this->env;
    }

    //Need this for google app engine / SLIM to play nice
    public function isRemote() {
        return $this->isRemote;
    }

    public function serverUrl() {
        if($this->isRemote()) {
            return "http://miyagiiturbo.herokuapp.com";
        }
        else {
            return "http://localhost:8080";
        }
    }
}

AppConfig::AppConfigInstance();

// Create a simple "default" Doctrine ORM configuration for Annotations

class EM
{
    private $entityManager;
    private $isDevMode = true;
    private $config = null;

    function __construct() {

        $env = AppConfig::AppConfigInstance()->Environment();

        $paths = array(__DIR__ . '/gw');
        $conn = EM::connectionFactory($env);

        $this->config = Setup::createConfiguration($this->isDevMode);
        $driver = new AnnotationDriver(new AnnotationReader(), $paths);

        // registering noop annotation autoloader - allow all annotations by default
        AnnotationRegistry::registerLoader('class_exists');
        $this->config->setMetadataDriverImpl($driver);

        if($this->entityManager == null)
            $this->entityManager = EntityManager::create($conn, $this->config);
    }

    public function EntityManager(){
        if ($this->entityManager == null) throw new \Exception("EntityManager was not constructed properly");

        return $this->entityManager;
    }

    public function Config() {
        if ($this->config == null) throw new \Exception("EntityManager was not constructed properly");

        return $this->config;
    }

    private static function connectionFactory($env){
        switch($env){
            //MAMP MySQL
            case "dev":
                return array (
                    'driver'        => 'pdo_mysql',
                    'user'          => 'raj',
                    'password'      => 'okinawazxsw23',
                    'dbname'        => 'okinawa',
                    'unix_socket'   => '/Applications/MAMP/tmp/mysql/mysql.sock',
                    'port'          => '8889',
                );
                break;
            //Local MySQL
            case "dev2":
                return array (
                    'driver'   => 'pdo_mysql',
                    'user'     => 'raj',
                    'host'     => 'localhost',
                    'port'     => '3306',
                    'dbname'   => 'okinawa',
                    'password' => 'okinawazxsw23'
                );
                break;
            //Heroku
            case "prod":
                $url=parse_url(getenv("CLEARDB_DATABASE_URL"));
                return array(
                    'driver' => 'pdo_mysql',
                    'user' => $url["user"],
                    'host' => $url["host"],
                    'dbname' => substr($url["path"],1),
                    'password' => $url["pass"]
                );
            // mysql:unix_socket=/cloudsql/<your-project-id>:<your-instance-name>;dbname=<database-name>',
            //Google App Engine
            case "prod2":
                return array (
                    'driver'        => 'pdo_mysql',
                    'user'          => 'root',
                    'dbname'        => 'okinawa',
                    'unix_socket'   => '/cloudsql/miyagii-turbo:okinawa',
                    'port'          => '3306',
                );
                break;
        }
    }
}

class RedisCloud {
    private static $RedisCloudInstance;
    private $redisuri;
    private static $rcache;

    private function __construct($env) {
        Autoloader::register(); //Predis configuration
        switch($env){
            case "dev":
                //TODO: remove this from git ...
                $this->redisuri = "redis://rediscloud:IeAbsxp6Ao6ytm7W@pub-redis-14665.us-east-1-4.4.ec2.garantiadata.com:14665";
                break;
            case "prod":
                $this->redisuri = $_ENV['REDISCLOUD_URL'];
        }
        self::$rcache = new Client(array(
            'host' => parse_url($this->redisuri, PHP_URL_HOST),
            'port' => parse_url($this->redisuri, PHP_URL_PORT),
            'password' => parse_url($this->redisuri, PHP_URL_PASS),
        ));
    }

    public static function RedisClient () {
        if (!self::$RedisCloudInstance) {
            self::$RedisCloudInstance = new RedisCloud(AppConfig::AppConfigInstance()->Environment());
        }
        return self::$rcache;
    }
}