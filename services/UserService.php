<?php
/**
 * Created by PhpStorm.
 * User: Gotham
 * Date: 14-12-01
 * Time: 5:48 PM
 */

namespace Miyagiiweb\services;

require_once __DIR__ . '/../bootstrap.php';

use Miyagiiweb\app\EM;
use Miyagiiweb\utils\MiyagiiSec;

class UserService {
    private $EM;

    function __construct(EM $em) {
        $this->EM = $em;
    }

    public function getUser ($username) {
        $aUser = $this->EM->EntityManager()->getRepository('User')->findOneBy(array('providerId' => $username[2]));

        return $aUser;
    }

    public function getEmailers () {
        $foundUsers = $this->EM->EntityManager()->getRepository('User')->findBy(array('userEmailFlag' => 1));
        return $foundUsers;
    }

    public function getUserByProviderAndId ($provider, $id) {
        $aUser = $this->EM->EntityManager()->getRepository('User')->findOneBy(array('providerName' => $provider, 'providerId' => $id));

        return $aUser;
    }

    public function createUser ($input) {
        $user = $this->getUserByProviderAndId($input->provider, $input->identifier);

        $email = "";
        if ($input->email != null) {
            $email = $input->email;
        }

        if(!$user) {
            $msec = new MiyagiiSec();
            $user = new \User();
            $user->setUserEmail($email);
            $user->setProviderDisplayName($input->displayName);
            $user->setUsername($input->displayName);
            $user->setProviderFirstname($input->firstName);
            $user->setProviderLastname("");
            $user->setProviderName($input->provider);
            $user->setProviderId($input->identifier);
            $user->setPassword($msec->generateUserHash2($input->provider, $input->displayName, $input->identifier));
            $this->EM->EntityManager()->persist($user);
            $this->EM->EntityManager()->flush();
        }

        return $user;
    }
}