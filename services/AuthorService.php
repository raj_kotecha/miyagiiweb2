<?php
/**
 * Created by PhpStorm.
 * User: Gotham
 * Date: 14-12-01
 * Time: 4:57 PM
 */

namespace Miyagiiweb\services;

require_once __DIR__ . '/../bootstrap.php';

    use Miyagiiweb\app\EM;
    use Author;

class AuthorService {
    private $EM;

    function __construct(EM $em) {
        $this->EM = $em;
    }

    public function addAuthor($author_name, $page_url) {

        $parse = parse_url($page_url);
        $author_url = $parse['host'];
        $foundAuthor = $this->EM->EntityManager()->getRepository('Author')->findOneBy(array('authorName' => $author_name, 'authorUrl' => $author_url));

        if (empty($foundAuthor)) {
            $new_author = new Author();
            $new_author->setAuthorName($author_name);
            $new_author->setAuthorUrl($author_url);

            $this->EM->EntityManager()->persist($new_author);
            return $new_author;
        }

        return $foundAuthor;
    }

    public function updateAuthor($author_name, $page_url) {

        if ($page_url == '') return null;
        $parse = parse_url($page_url);
        $author_url = $parse['host'];
        $foundAuthor = $this->EM->EntityManager()->getRepository('Author')->findOneBy(array('authorUrl' => $author_url));

        if (!empty($foundAuthor)) {
            $foundAuthor->setAuthorName($author_name);
            $foundAuthor->setAuthorUrl($author_url);

            $this->EM->EntityManager()->persist($foundAuthor);
            return $foundAuthor;
        }
        return null;
    }

    public function getAuthorsByURL ($page_url) {
        $parse = parse_url($page_url);
        $author_url = $parse['host'];
        $foundAuthor = $this->EM->EntityManager()->getRepository('Author')->findBy(array('authorUrl' => $author_url));
        return $foundAuthor;
    }
}