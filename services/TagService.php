<?php
/**
 * Created by PhpStorm.
 * User: Gotham
 * Date: 14-12-01
 * Time: 10:20 AM
 */

namespace Miyagiiweb\services;

require_once __DIR__ . '/../bootstrap.php';

use Miyagiiweb\app\EM;
use Tag;
use Doctrine\ORM\Query;


class TagService
{
    private $EM;

    function __construct(EM $em) {
        $this->EM = $em;
    }
    //TODO: This likely needs to be optimized!
    public function addTags($tags)
    {
        $processed_tags = [];

        foreach ($tags as $tag) {
            $foundtag = $this->EM->EntityManager()->getRepository('Tag')->findOneBy(array('tagName' => $tag));

            if (empty($foundtag)) {
                $new_tag = new Tag();
                $new_tag->setTagName($tag);
                $this->EM->EntityManager()->persist($new_tag);
                array_push($processed_tags, $new_tag);
            } else {
                array_push($processed_tags, $foundtag);
            }
        }

        return $processed_tags;
    }

    public function getTagsForUser ($userinfo) {

        $us = new UserService($this->EM);
        $aUser = $us->getUser($userinfo);

        $qb = $this->EM->EntityManager()->createQueryBuilder();

        $query = $qb->select('t.tagId', 't.tagName')
            ->from('SnippetTag', 'st')
            ->distinct(true)
            ->innerJoin('st.tag', 't')
            ->innerJoin('st.snippet', 's')
            ->andWhere('s.user = :auser')
            ->orderBy('t.tagName', 'ASC')
            ->setParameter('auser', $aUser)
            ->getQuery();

        $result = $query->getResult();

        return $result;
    }

    public function getSnippetTags ($snippet_id) {

        $qb = $this->EM->EntityManager()->createQueryBuilder();

        $sc = new SnippetService($this->EM);

        $snippet = $sc->getSingleSnippet($snippet_id);

        $query = $qb->select('t.tagName')
            ->from ('SnippetTag', 'st')
            ->innerJoin('st.tag', 't')
            ->andWhere('st.snippet = :snippet')
            ->setParameter('snippet', $snippet)
            ->getQuery();

        $result = $query->getResult();

        return $result;

    }

    public function removeSnippetTags ($snippet) {
        $qb = $this->EM->EntityManager()->createQueryBuilder();

        $query = $qb->delete('SnippetTag', 'st')
            ->andWhere('st.snippet = :snippet')
            ->setParameter('snippet', $snippet)
            ->getQuery();

        $result = $query->execute();

        return $result;
    }
}