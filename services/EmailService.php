<?php

namespace Miyagiiweb\services;

require_once __DIR__ . '/../bootstrap.php';

use Miyagiiweb\app\EM;
use SendGrid;

class EmailService {

    public function jog($testMode) {
        $testCheck = null;
        if ($testMode) {
            $sc = new SnippetService(new EM());
            $uc = new UserService(new EM());

            $emailers = $uc->getEmailers();

            //TODO: Performance issue here
            foreach ($emailers as $mailer) {
                $username = $mailer->getUsername();
                $userEmail = $mailer->getUserEmail();

                //Doesn't work because you randidx is not a snippetid so reverting to original logic for now
                //$snippetCount = $sc->getSnippetCountForUser($username);
                //$randidx = rand(0, $snippetCount - 1);
                //check out: http://akinas.com/pages/en/blog/mysql_random_row/ for ideas on how to improve this logic

                $snippets = $sc->getSnippetsByUser($username);
                $randidx = rand(0, count($snippets) - 1);
                $randsnippet = $snippets[$randidx];

                $sgu = $_ENV['SENDGRID_USERNAME'];
                $sgp = $_ENV['SENDGRID_PASSWORD'];

                $sendgrid = new SendGrid($sgu, $sgp);
                $message = new SendGrid\Email();
                $message->addTo($userEmail)->
                setFrom('jog@marqable.com')->
                setSubject('Jog your memory test')->
                setText($randsnippet["snippetDetail"] . '\n' . $randsnippet["snippetDetail"])->
                setHtml('<div style="width:400px;" ><p style="font-size: 1.8rem; font-family: Garamond, "Times New Roman", Times, Serif">' . $randsnippet["snippetComment"] .
                    '</p><br/><p style="font-style: italic; color: grey; background-color:#E8F8FF">' . $randsnippet["snippetComment"] .
                    '</p><p style="font-size: .5rem; font-family: Garamond, "Times New Roman", Times, Serif">'. $randsnippet["snippetPageurl"] .'</p></div>');
                $response = $sendgrid->send($message);

            }

            //echo json_encode($response);
        }
        else
        {
            $randDoIt = rand(0, 100);
            if ($randDoIt > 50) {
                //TODO: Refactor as follows
                //4. Make sure the whole thing is async and use a queue if necessary (i.e. if sendgrid can't do async)
                $sc = new SnippetService(new EM());
                $uc = new UserService(new EM());

                $emailers = $uc->getEmailers();

                //TODO: Performance issue here
                foreach ($emailers as $mailer) {
                    $username = $mailer->getUsername();
                    $userEmail = $mailer->getUserEmail();
                    $userid = $mailer->getProviderId();

                    /*
                     * TODO: current services depend on requests coming from a web context, where COOKIE->ID plays nice
                     * with Redis to get a userinfo object. Email requests don't have this context ... yet ...
                     */
                    $userinfo = array(null, null, $userid);

                    $snippets = $sc->getSnippetsByUser($userinfo);
                    $sCount = count($snippets);

                    if($sCount > 9) {
                        $randidx = rand(0, $sCount - 1);
                        $randsnippet = $snippets[$randidx];
                        $randHours = rand(9, 21);

                        $sgu = $_ENV['SENDGRID_USERNAME'];
                        $sgp = $_ENV['SENDGRID_PASSWORD'];

                        $sendgrid = new SendGrid($sgu, $sgp);
                        $message = new SendGrid\Email();
                        $message->addTo($userEmail)->
                        setFrom('jog@marqable.com')->
                        setSubject('Remember this?')->
                        setSendAt(time() + ($randHours * 60 * 60))->
                        setText($randsnippet["snippetDetail"] . '\n' . $randsnippet["snippetDetail"])->
                        setHtml('<div style="width:400px;" ><p style="font-size: 1.8rem; font-family: Garamond, "Times New Roman", Times, Serif">' . nl2br($randsnippet["snippetComment"]) .
                            '</p><br/><p style="font-style: italic; color: grey; background-color:#E8F8FF">' . nl2br($randsnippet["snippetDetail"]) .
                            '</p><p style="font-size: .5rem; font-family: Garamond, "Times New Roman", Times, Serif">' . $randsnippet["snippetPageurl"] . '</p></div>');
                        $response = $sendgrid->send($message);
                    }
                }
                //echo json_encode($response);
            }
            /*
            else {
                $sgu = $_ENV['SENDGRID_USERNAME'];
                $sgp = $_ENV['SENDGRID_PASSWORD'];
                $sendgrid = new SendGrid($sgu, $sgp);

                $message = new SendGrid\Email();
                $message->addTo('raj.kotecha@gmail.com')->
                setFrom('jog@marqable.com')->
                setSubject('Test Jog your memory')->
                setText('triggered jog but no dice:' . $randDoIt)->
                setHtml('<p>Triggered jog but no dice:' . $randDoIt . '</p>');

                $response = $sendgrid->send($message);
            }
            */
        }
    }
}