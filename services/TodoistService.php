<?php
/**
 * Created by PhpStorm.
 * User: Gotham
 * Date: 15-08-18
 * Time: 9:53 AM
 */

namespace Miyagiiweb\services;

use Miyagiiweb\services\CurlService;
use Flow\JSONPath\JSONPath;

class TodoistService {

    private $token;
    private $inboxId;

    function __construct($_token) {
        $this->token = $_token;
    }

    function getInboxId() {
        if ($this->inboxId == null) {
            $data = array('token' => $this->token,
                'seq_no' => '0',
                'seq_no_global' => '0',
                'resource_types' => '["projects"]');

            $cs = new CurlService();
            $projects = $cs->CallAPI("POST", "https://todoist.com/API/v6/sync", $data);

            $projectsArray = json_decode($projects);
            $js = new JSONPath($projectsArray);
            $this->inboxId = $js->find('$..Projects[?(@.name="Inbox")]')->find('$..id')->data()[0];
        }

        return $this->inboxId;
    }

    function addItem ($itemText) {
        $commands = '[{"type": "item_add", "temp_id": "' . uniqid() . '", "uuid": "' . uniqid() . '", "args": {"content": "' . $itemText . '", "project_id": ' . $this->getInboxId() . '}}]';

        $data = array('token' => $this->token, 'commands' => $commands);

        $cs = new CurlService();
        $action = $cs->CallAPI("POST", "https://todoist.com/API/v6/sync", $data);

        return $action;
    }

    function Sync () {
        $data = array('token'=>$this->token,
            'seq_no'=>'0',
            'seq_no_global'=>'0',
            'resource_types'=>'["all"]');

        $cs = new CurlService();
        $retval = $cs->CallAPI("POST", "https://todoist.com/API/v6/sync", $data);
        return $retval;
    }
} 