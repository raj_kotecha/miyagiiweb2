<?php namespace Miyagiiweb\services;

require_once __DIR__ . '/../bootstrap.php';

use Miyagiiweb\app\EM;
use Miyagiiu;

class MiyagiiuService {
    public function allAction () {

        //echo "Im here now!";
        $EM = new EM();
        $records = $EM->EntityManager()
            ->getRepository('Miyagiiu')
            ->findAll();

        return $records;
    }

    /*
    public function addUser ($addUser) {
        $EM = new EM();
        $EM->EntityManager()->persist($addUser);
        $EM->EntityManager()->flush();
    }
    */

    public function getUser($apiKey) {
        $EM = new EM();
        $user = $EM->EntityManager()
            ->getRepository('Miyagiiu')
            ->findOneBy(array('apiKey' => $apiKey));

        return $user;
    }
}