<?php
/**
 * Created by PhpStorm.
 * User: Gotham
 * Date: 15-02-16
 * Time: 11:42 AM
 */

namespace Miyagiiweb\services;

use Miyagiiweb\app\EM;
use Miyagiiweb\app\RedisCloud;
use Focus;

class FocalService {

    private $EM;

    function __construct(EM $em) {
        $this->EM = $em;
    }

    function getFocalsForUser($userinfo) {
        $us = new UserService($this->EM);
        $aUser = $us->getUser($userinfo);

        $qb = $this->EM->EntityManager()->createQueryBuilder();

        $query = $qb->select('f.focusId', 'f.focusName', 'f.focusCreatedAt')
            ->from('Focus', 'f')
            ->Where('f.user = :auser')
            ->orderBy('f.focusId', 'ASC')
            ->setParameter('auser', $aUser)
            ->getQuery();

        $result = $query->getResult();

        return $result;
    }

    function getUserFocalById($userinfo, $focalId) {
        $us = new UserService($this->EM);
        $aUser = $us->getUser($userinfo);
        //$userId = $aUser->getUserId();

        $focal = $this->EM->EntityManager()->getRepository('Focus')->findOneBy(array('user' => $aUser, 'focusId' => $focalId));

        return $focal;
    }

    function createFocal($focalinfo, $user) {

        $uc = new UserService($this->EM);
        $userinfo = json_decode(RedisCloud::RedisClient()->get($user));
        $aUser = $uc->getUser($userinfo);

        $focus = new Focus();
        $focus->setFocusName($focalinfo);
        $focus->setUser($aUser);

        $this->EM->EntityManager()->persist($focus);

        $this->EM->EntityManager()->flush();
        return $focus;
    }

    function updateFocal($focalId, $userinfo) {

    }

    function deleteFocal($focalId, $userinfo) {

    }
}