<?php
/**
 * Created by PhpStorm.
 * User: Gotham
 * Date: 15-08-16
 * Time: 5:49 AM
 */

namespace Miyagiiweb\services;
use Miyagiiweb\app\EM;

class ActionService {
    private $EM;

    function __construct(EM $em) {
        $this->EM = $em;
    }

    function parseActions ($stringToParse) {
        $regex = '/\[(.*?):(.*?)\]/';
        preg_match_all($regex, $stringToParse, $matches, PREG_SET_ORDER);

        $ts = new TodoistService("");

        foreach ($matches as $match) {
            if (count($match) == 3) {
                $action = $match[1] . ": " . $match[2];
                $this->addAction("TODOIST", $action);
            }
        }

        return $matches;
    }

    function addAction($actionService, $action) {

        switch ($actionService)
        {
            case "TODOIST":
                //TODO: replace hard coded token with user defined token
                $ts  = new TodoistService("5df04425cef4f4c53dc031680458ce4d9224ff8d");
                $ts->addItem($action);
                break;
            default:
                break;
        }
    }
} 