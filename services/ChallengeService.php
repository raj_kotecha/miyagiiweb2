<?php

namespace Miyagiiweb\services;

use Miyagiiweb\app\EM;
use Miyagiiweb\app\RedisCloud;
use Challenge;
//use ChallengeType;

class ChallengeService
{

    private $EM;

    function __construct(EM $em)
    {
        $this->EM = $em;
    }

    function getChallengesForUser($userinfo) {
        $us = new UserService($this->EM);
        $aUser = $us->getUser($userinfo);

        $qb = $this->EM->EntityManager()->createQueryBuilder();

        $query = $qb->select('c.challengeId', 'c.challengeDescription', 'f.challengeCreatedAt')
            ->from('Challenge', 'c')
            ->Where('c.user = :auser')
            ->orderBy('c.challengeId', 'ASC')
            ->setParameter('auser', $aUser)
            ->getQuery();

        $result = $query->getResult();

        return $result;
    }

    function getUserChallengeById($userinfo, $challengeId) {
        $us = new UserService($this->EM);
        $aUser = $us->getUser($userinfo);

        $challenge = $this->EM->EntityManager()->getRepository('Challenge')->findOneBy(array('user' => $aUser, 'challengeId' => $challengeId));

        return $challenge;
    }

    function getChallengesBySnippet($userinfo, $snippetId) {
        $us = new UserService($this->EM);
        $aUser = $us->getUser($userinfo);

        $ss = new SnippetService($this->EM);
        $snippet = $ss->getSnippet($snippetId);

        //TODO: Filter out non-type-1 challenges
        $challenge = $this->EM->EntityManager()->getRepository('Challenge')->findBy(array('user' => $aUser, 'snippet' => $snippet));
        return $challenge;
    }

    function createChallenge($challengeInfo) {

        $uc = new UserService($this->EM);
        $userinfo = json_decode(RedisCloud::RedisClient()->get($challengeInfo->username));
        $aUser = $uc->getUser($userinfo);

        $challengeTypeId = explode("_", $challengeInfo->challengeType)[1];

        $challengeType = $this->EM->EntityManager()->getRepository('ChallengeType')->find($challengeTypeId);

        $focus = $this->EM->EntityManager()->getRepository('Focus')->find($challengeInfo->focusId);

        $snippet = $this->EM->EntityManager()->getRepository('Snippet')->find($challengeInfo->snippetId);

        $challenge = new Challenge();
        $challenge->setChallengeDescription($challengeInfo->challengeDescription);
        $challenge->setChallengeType($challengeType);
        $challenge->setFocus($focus);
        $challenge->setUser($aUser);
        $challenge->setSnippet($snippet);

        $this->EM->EntityManager()->persist($challenge);

        $this->EM->EntityManager()->flush();
        return $challenge;
    }

    function updateChallenge($challengeId, $userinfo) {

    }

    function deleteChallenge($challengeId, $userinfo) {

    }
}