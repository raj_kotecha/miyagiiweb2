<?php
/**
 * Created by PhpStorm.
 * User: Gotham
 * Date: 15-03-22
 * Time: 6:12 AM
 */

namespace Miyagiiweb\services;
use Miyagiiweb\app\RedisCloud;
use ChallengeIdeaLookup;
use Idea;
use Miyagiiweb\app\EM;

class IdeaService {
    private $EM;

    function __construct(EM $em) {
        $this->EM = $em;
    }

    function addIdea ($ideainfo) {
        $uc = new UserService($this->EM);
        $userinfo = json_decode(RedisCloud::RedisClient()->get($ideainfo->username));
        $aUser = $uc->getUser($userinfo);

        $cs = new ChallengeService($this->EM);
        $challenge = $cs->getUserChallengeById($userinfo, $ideainfo->challengeId);

        $idea = new Idea();
        $idea->setUser($aUser);
        $idea->setIdeaTitle($ideainfo->ideaTitle);
        $this->EM->EntityManager()->persist($idea);

        $lookup = new ChallengeIdeaLookup();
        $lookup->setChallenge($challenge);
        $lookup->setIdea($idea);

        $this->EM->EntityManager()->persist($lookup);

        $this->EM->EntityManager()->flush();
        return $idea;
    }

    function getIdeasForChallenge ($userinfo, $challengeId) {
        $uc = new UserService($this->EM);
        $aUser = $uc->getUser($userinfo);

        $cs = new ChallengeService($this->EM);
        $challenge = $cs->getUserChallengeById($userinfo, $challengeId);

        $qb = $this->EM->EntityManager()->createQueryBuilder();

        $query = $qb->select('i.ideaTitle')
            ->from ('ChallengeIdeaLookup', 'clu')
            ->innerJoin('clu.idea', 'i')
            ->andWhere('clu.challenge = :challenge')
            ->andWhere('i.user = :auser')
            ->orderBy('i.ideaId', 'ASC')
            ->setParameter('challenge', $challenge)
            ->setParameter('auser', $aUser)
            ->getQuery();

        $result = $query->getResult();

        return $result;
    }
}