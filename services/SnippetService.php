<?php
/**
 * Created by PhpStorm.
 * User: Gotham
 * Date: 14-12-01
 * Time: 7:15 AM
 */

namespace Miyagiiweb\services;

require_once __DIR__ . '/../bootstrap.php';

use Miyagiiweb\app\AppConfig;
use Miyagiiweb\app\EM;
use Miyagiiweb\app\RedisCloud;
use Snippet;
use SnippetTag;
use UserSnippet;
use Miyagiiweb\services\ActionService;

class SnippetService {

    private $EM;

    function __construct(EM $em) {
        $this->EM = $em;
    }

    public function addSnippet ($snippet_info) {
        $ac = new AuthorService($this->EM);
        $author = $ac->addAuthor($snippet_info->author, $snippet_info->page_url);
        $this->EM->EntityManager()->persist($author);

        $uc = new UserService($this->EM);
        $aUser = $uc->getUserByProviderAndId($snippet_info->provider, $snippet_info->user);

        $snippet = new Snippet();
        $snippet->setSnippetComment($snippet_info->comment);
        $snippet->setSnippetDetail($snippet_info->info);
        $snippet->setSnippetPageurl($snippet_info->page_url);
        $snippet->setSnippetAuthor($author);
        $snippet->setUser($aUser);
        $this->EM->EntityManager()->persist($snippet);

        $userSnippet = new UserSnippet();
        $userSnippet->setSnippet($snippet);
        $userSnippet->setUser($aUser);
        $userSnippet->setUserSnippetOwnerId(0);
        $this->EM->EntityManager()->persist($userSnippet);

        $tc = new TagService($this->EM);
        $tag_list = $tc->addTags(explode(",", $snippet_info->tags));
        foreach ($tag_list as $stag) {
            $snippet_tag = new SnippetTag();
            $snippet_tag->setSnippet($snippet);
            $snippet_tag->setTag($stag);
            $this->EM->EntityManager()->persist($snippet_tag);
        }

        $this->EM->EntityManager()->flush();
        return $snippet;
    }

    public function updateSnippet ($snippet_info) {
        try {
            $uc = new UserService($this->EM);
            $userinfo = json_decode(RedisCloud::RedisClient()->get($snippet_info->username));
            //TODO: Make sure you can never get a DB entity without providing a user ID esp. for things like snippets
            $aUser = $uc->getUser($userinfo);

            $snippet = $this->getSingleSnippet($snippet_info->snippet_id);



            $author = $snippet->getSnippetAuthor();
            $author->setAuthorName($snippet_info->author);

            $snippet->setSnippetComment($snippet_info->comment);
            $snippet->setSnippetDetail($snippet_info->info);
            if($author != null) $snippet->setSnippetAuthor($author);
            $this->EM->EntityManager()->persist($snippet);

            $tc = new TagService($this->EM);
            $tc->removeSnippetTags($snippet);

            $tag_list = $tc->addTags(explode(",", $snippet_info->tags));
            foreach ($tag_list as $stag) {
                $snippet_tag = new SnippetTag();
                $snippet_tag->setSnippet($snippet);
                $snippet_tag->setTag($stag);
                $this->EM->EntityManager()->persist($snippet_tag);
            }

            $this->EM->EntityManager()->flush();
            return $snippet;
        }
        catch (\Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    public function addSnippetX ($snippet_info) {
        $url = AppConfig::AppConfigInstance()->serverUrl();
        $ac = new AuthorService($this->EM);
        $author = $ac->addAuthor($snippet_info->author, $url);
        $this->EM->EntityManager()->persist($author);

        $uc = new UserService($this->EM);

        $userinfo = json_decode(RedisCloud::RedisClient()->get($snippet_info->username));

        $aUser = $uc->getUser($userinfo);

        if ($aUser->getUserAutoAction() == 1) {
            $as = new ActionService($this->EM);
            $actions = $as->parseActions($snippet_info->comment);
        }

        $snippet = new Snippet();
        $snippet->setSnippetComment($snippet_info->comment);
        $snippet->setSnippetDetail($snippet_info->info);
        $snippet->setSnippetPageurl($url);
        $snippet->setSnippetAuthor($author);
        $snippet->setUser($aUser);
        $this->EM->EntityManager()->persist($snippet);

        $userSnippet = new UserSnippet();
        $userSnippet->setSnippet($snippet);
        $userSnippet->setUser($aUser);
        $userSnippet->setUserSnippetOwnerId(0);
        $this->EM->EntityManager()->persist($userSnippet);

        $tc = new TagService($this->EM);
        $tag_list = $tc->addTags(explode(",", $snippet_info->tags));
        foreach ($tag_list as $stag) {
            $snippet_tag = new SnippetTag();
            $snippet_tag->setSnippet($snippet);
            $snippet_tag->setTag($stag);
            $this->EM->EntityManager()->persist($snippet_tag);
        }

        $this->EM->EntityManager()->flush();
        $retval = array($snippet, $actions);
        return $retval;
    }

    public function getSingleSnippet ($snippet_id){
        $snippet = $this->EM->EntityManager()
            ->getRepository('Snippet')
            ->find($snippet_id);

        return $snippet;
    }

    public function getSnippet ($snippet_id, $userinfo) {
        //$snippet = $this->EM->EntityManager()
        //    ->getRepository('Snippet')
        //    ->find($snippet_id);

        //return $snippet;
            $us = new UserService($this->EM);
            $aUser = $us->getUser($userinfo);

            $qb = $this->EM->EntityManager()->createQueryBuilder();

            $query = $qb->select('s.snippetId', 's.snippetComment', 's.snippetDetail', 's.snippetPageurl', 'a.authorName', 's.snippetCreatedAt')
                ->from('Snippet', 's')
                ->innerJoin('s.snippetAuthor', 'a')
                ->innerJoin('s.user', 'u')
                ->andWhere('s.user = :auser')
                ->andWhere('s.snippetId = :sid')
                ->orderBy('s.snippetId', 'DESC')
                ->setParameter('auser', $aUser)
                ->setParameter('sid', $snippet_id)
                ->getQuery();

            $result = $query->getResult();

            return $result;
    }

    //Returns snippets for a given user
    //NOTES:
    //1. $start is currently not used, but can be brought into play by adding
    //->setMaxResults(10)
    //->setFirstResult($start)
    //to the query in order to limit the number of records returned
    public function getSnippetsByUser ($userinfo, $start = 0) {
        $us = new UserService($this->EM);
        $aUser = $us->getUser($userinfo);

        $qb = $this->EM->EntityManager()->createQueryBuilder();

        $query = $qb->select('s.snippetId', 's.snippetComment', 's.snippetDetail', 's.snippetPageurl', 'a.authorName', 's.snippetCreatedAt')
            ->from('Snippet', 's')
            ->innerJoin('s.snippetAuthor', 'a')
            ->innerJoin('s.user', 'u')
            ->andWhere('s.user = :auser')
            ->orderBy('s.snippetId', 'DESC')
            ->setParameter('auser', $aUser)
            ->getQuery();

        $result = $query->getArrayResult();

        return $result;
    }

    public function getSnippetCountForUser ($username) {
        $us = new UserService($this->EM);
        $aUser = $us->getUser($username);

        $qb = $this->EM->EntityManager()->createQueryBuilder();

        $query = $qb->select('count(s.snippetId)')
            ->from('Snippet', 's')
            ->innerJoin('s.user', 'u')
            ->andWhere('s.user = :auser')
            ->setParameter('auser', $aUser)
            ->getQuery();

        $result = $query->getSingleScalarResult();

        return $result;

    }

    /*
    public function getSnippetsByUserTags ($userinfo, $tags) {

        $us = new UserService($this->EM);
        $aUser = $us->getUser($userinfo);

        $qb = $this->EM->EntityManager()->createQueryBuilder();

        $query = $qb->select('s.snippetComment', 's.snippetDetail', 's.snippetPageurl', 'a.authorName', 's.snippetCreatedAt', 's.snippetId')
            ->from('Snippet', 's')
            ->distinct(true)
            ->innerJoin('s.snippetAuthor', 'a')
            ->innerJoin('s.user', 'u')
            ->innerjoin('s.snippetTag', 't')
            ->andWhere('s.user = :auser')
            ->andWhere('t.tag IN (:tags)')
            ->orderBy('s.snippetId', 'DESC')
            ->setParameter('auser', $aUser)
            ->setParameter('tags', $tags)
            ->getQuery();

        $result = $query->getResult();

        return $result;
    }
    */

    public function getSnippetsByUserTagsX ($userinfo, $tags) {
        $us = new UserService($this->EM);
        $aUser = $us->getUser($userinfo);

        $qb = $this->EM->EntityManager()->createQueryBuilder();

        $query = $qb->select('s.snippetId', 's.snippetComment', 's.snippetDetail', 's.snippetPageurl', 'a.authorName', 's.snippetCreatedAt', 's.snippetId')
            ->from ('SnippetTag', 'st')
            ->innerJoin('st.snippet', 's')
            ->innerJoin('s.snippetAuthor', 'a')
            ->andWhere('s.user = :auser')
            ->andWhere('st.tag IN (:tags)')
            ->orderBy('s.snippetId', 'DESC')
            ->setParameter('auser', $aUser)
            ->setParameter('tags', $tags)
            ->getQuery();

        $result = $query->getResult();

        return $result;
    }

    public function deleteSnippet ($snippetInfo) {
        $uc = new UserService($this->EM);
        $userinfo = json_decode(RedisCloud::RedisClient()->get($snippetInfo->username));
        //TODO: Add security
        $aUser = $uc->getUser($userinfo);

        $aSnippet = $this->EM->EntityManager()->getRepository('Snippet')->findOneBy(array('snippetId' => $snippetInfo->snippet));
        $this->EM->EntityManager()->remove($aSnippet);
        $this->EM->EntityManager()->flush();
    }
} 